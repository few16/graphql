import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import { execute, subscribe } from 'graphql';
import * as http from 'http';
import { Server } from 'net';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { graphQLSchema } from './schema';

const isProd = process.env.NODE_ENV === 'production';
const isTest = process.env.NODE_ENV === 'test';

const APP_PORT = process.env.APP_PORT || 3443;
const WS_HOST = process.env.WS_HOST || 'ws://localhost:3443';

// create express instance
export const app = express();

app.use(cookieParser());

// Configuration
app.set('port', APP_PORT);

// Security Stuff
app.use(cors());

// GraphQL Endpoint
app.use(
  '/graphql',
  bodyParser.json(),
  graphqlExpress((req) => ({
    schema: graphQLSchema,
    subscriptionsEndpoint: `${WS_HOST}/subscriptions`,
    tracing: !(isProd || isTest),
    cacheControl: isProd
  }))
);

if (!(isProd || isTest)) {
  app.use(
    '/graphiql',
    graphiqlExpress({
      subscriptionsEndpoint: `${WS_HOST}/subscriptions`,
      endpointURL: '/graphql'
    })
  );
}

// Create Express server.
export const server = http.createServer(app);

/**
 * Callback to be called after server kick off
 */
function serverCallback() {
  new SubscriptionServer(
    { schema: graphQLSchema, execute, subscribe },
    { path: '/subscriptions', server }
  );
  if (!isTest) {
    /* tslint:disable */
    console.log(
      'App is running at http://localhost:%d in %s mode',
      app.get('port'),
      app.get('env')
    );
    console.log('Press CTRL-C to stop\n');
    /* tslint:enable */
  }
}

/**
 * Run http server
 *
 * @export
 * @returns {Server} server instance
 */
export function startServer(): Server {
  return server.listen(app.get('port'), serverCallback);
}
