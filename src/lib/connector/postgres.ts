import * as SequelizeI from 'sequelize';
import { Sequelize } from 'sequelize';

const POSTGRES_DATABASE = process.env.POSTGRES_DATABASE || 'school';
const POSTGRES_HOST = process.env.POSTGRES_HOST || 'localhost';
const POSTGRES_PASSWORD = process.env.POSTGRES_PASSWORD || null;
const POSTGRES_PORT = Number(process.env.POSTGRES_PORT) || 5432;
const POSTGRES_USERNAME = process.env.POSTGRES_USERNAME || 'postgres';
const POSTGRES_TIMEZONE = process.env.POSTGRES_TIMEZONE || 'Asia/Jakarta';

export const db: Sequelize = new SequelizeI(
  POSTGRES_DATABASE,
  POSTGRES_USERNAME,
  POSTGRES_PASSWORD,
  {
    host: POSTGRES_HOST,
    port: POSTGRES_PORT,
    dialect: 'postgres',
    logging: false,
    timezone: POSTGRES_TIMEZONE,
    operatorsAliases: false,
    pool: {
      max: 5,
      min: 0,
      idle: 2000
    }
  }
);
