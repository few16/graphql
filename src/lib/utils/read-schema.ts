import { readFileSync } from 'fs';
import { resolve } from 'path';

/**
 * Read graphql schema from disk to be parsed
 *
 * @export
 * @param {string} schema schema path relative to directory
 * @param {string} dir directory relative to usually __dirname
 * @returns {string}
 */
export function readSchema(schema: string, dir: string): string {
  return readFileSync(
    resolve(dir, schema),
    'utf8'
  );
}
